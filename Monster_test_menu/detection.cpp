#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "monster.h"
#include "sdl_fonction.h"
using namespace std;
/****************** Nom de la fonction **********************
* detectionBas                                              *
******************** Auteur , Dates *************************
*  Auteur: Rémy Guitou     Date: 07/12/2016                 *
********************* Description ***************************
* Cette fonction indique à un monstre se déplacant vers le  *
* bas quelle reaction adopter en fonction de ce qui se      *
* trouve sur son chemin                                     *
*********************** Entrées *****************************
* Cette fonction agit sur les valeurs de la matrice d'un    *
* niveau et le déplacement d'un monstre                     *
*********************** Sorties *****************************
* Cette fonction ne retourne aucune information mais        *
* peut  modifier les conditions de déplacement d'un         *
* monstre, la présence de glaces, la position et la         *
* direction d'un monstre                                    *
************************************************************/
void detectionBas(TGrille &grille, int p, int &l, int &tempy, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas)
{
    if(grille[p][l+1] == 2)
    {
        grille[p][l+1] = 1;
    }

    if(grille[p][l+1] == 4)
    {
        grille[p][l+1] = 0;
        collision = true;
    }
    if(grille[p][l+1] == 5)
    {
        grille[p][l] = 0;
        grille[p][l+1]=1;
        l++;
        droite = true;
        collision = true;
        grille[p][l]=5;
    }
    if(grille[p][l+1] == 6)
    {
        grille[p][l] = 0;
        grille[p][l+1]=1;
        l++;
        gauche = true;
        collision = true;
        grille[p][l]=6;
    }
    if(grille[p][l+1] == 7)
    {
        tempy = l;
        grille[p][l] = 0;
        haut = true;
        collision = true;
    }
    if(grille[p][l+1] == 8)
    {
        grille[p][l] = 0;
        grille[p][l+1]=1;
        l++;
        bas = true;
        collision = true;
        grille[p][l]=8;
    }
}
/****************** Nom de la fonction **********************
* detectionHaut                                             *
******************** Auteur , Dates *************************
*  Auteur: Rémy Guitou     Date: 07/12/2016                 *
********************* Description ***************************
* Cette fonction indique à un monstre se déplacant vers le  *
* haut quelle reaction adopter en fonction de ce qui se     *
* trouve sur son chemin                                     *
*********************** Entrées *****************************
* Cette fonction agit sur les valeurs de la matrice d'un    *
* niveau et le déplacement d'un monstre                     *
*********************** Sorties *****************************
* Cette fonction ne retourne aucune information mais        *
* peut  modifier les conditions de déplacement d'un         *
* monstre, la présence de glaces, la position et la         *
* direction d'un monstre                                    *
************************************************************/
void detectionHaut(TGrille &grille, int p,int &tempx, int &tempy, int &l, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas)
{
    if(grille[p][tempy-1] == 2)
    {
        grille[p][tempy-1] = 1;
    }
    if(grille[p][tempy-1] == 4)
    {
        grille[p][tempy-1] = 0;
        collision = true;
    }
    if(grille[p][tempy-1] == 5)
    {
        tempy--;
        grille[p][tempy+1]=0;
        l = tempy;
        tempx = p;
        droite = true;
        collision = true;
    }
    if(grille[p][tempy-1] == 6)
    {
        tempy--;
        grille[p][tempy+1]=0;
        l = tempy;
        tempx = p;
        gauche = true;
        collision = true;
    }
    if(grille[p][tempy-1] == 7)
    {
        grille[p][tempy-1] = 1;
        tempy--;
        grille[p][tempy] = 7;
        grille[p][tempy+1] = 0;
        haut = true;
        collision = true;

    }
    if(grille[p][tempy-1] == 8)
    {
        l = tempy;
        bas = true;
        grille[p][tempy] = 0;
        collision = true;
    }
}
/****************** Nom de la fonction **********************
* detectionDroite                                             *
******************** Auteur , Dates *************************
*  Auteur: Rémy Guitou     Date: 07/12/2016                 *
********************* Description ***************************
* Cette fonction indique à un monstre se déplacant vers la  *
* droite quelle reaction adopter en fonction de ce qui se   *
* trouve sur son chemin                                     *
*********************** Entrées *****************************
* Cette fonction agit sur les valeurs de la matrice d'un    *
* niveau et le déplacement d'un monstre                     *
*********************** Sorties *****************************
* Cette fonction ne retourne aucune information mais        *
* peut  modifier les conditions de déplacement d'un         *
* monstre, la présence de glaces, la position et la         *
* direction d'un monstre                                    *
************************************************************/
void detectionDroite(TGrille &grille, int &p,int &tempx, int &tempy, int &l, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas)
{
    if(grille[tempx+1][l] == 4)
    {
        grille[tempx+1][l] = 0;
        collision = true;
    }
    if(grille[tempx+1][l] == 2)
    {
        grille[tempx+1][l] = 1;
    }
    if(grille[tempx+1][l] == 5)
    {
        grille[tempx][l] = 0;
        tempx++;
        droite = true;
        collision = true;
    }
    if(grille[tempx+1][l] == 6)
    {
        p = tempx;
        grille[p][l] = 0;
        grille[p+1][l] = 1;
        gauche = true;
        grille[p][l] = 0;
        grille[p+1][l] = 6;
        collision = true;
    }
    if(grille[tempx+1][l] == 7)
    {
        tempy = l;
        p = tempx;
        grille[p][l]= 0;
        grille[p+1][l] = 1;
        p++;
        grille[p][l] = 7;
        haut = true;
        collision = true;
    }
    if(grille[tempx+1][l] == 8)
    {
        p = tempx;
        grille[p][l]= 0;
        grille[p+1][l] = 1;
        p++;
        bas = true;
        grille[p][l] = 8;
        collision = true;
    }
}
/****************** Nom de la fonction **********************
* detectionGauche                                           *
******************** Auteur , Dates *************************
*  Auteur: Rémy Guitou     Date: 07/12/2016                 *
********************* Description ***************************
* Cette fonction indique à un monstre se déplacant vers la  *
* gauche quelle reaction adopter en fonction de ce qui se   *
* trouve sur son chemin                                     *
*********************** Entrées *****************************
* Cette fonction agit sur les valeurs de la matrice d'un    *
* niveau et le déplacement d'un monstre                     *
*********************** Sorties *****************************
* Cette fonction ne retourne aucune information mais        *
* peut  modifier les conditions de déplacement d'un         *
* monstre, la présence de glaces, la position et la         *
* direction d'un monstre                                    *
************************************************************/
void detectionGauche(TGrille &grille, int &p,int &tempx, int &tempy, int &l, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas)
{
    if(grille[p-1][l] == 2)
    {
        grille[p-1][l] = 1;
    }
    if(grille[p-1][l] == 4)
    {
        grille[p-1][l] = 0;
        collision=true;

    }
    if(grille[p-1][l] == 5)
    {
        tempx = p;
        grille[p][l] = 0;
        grille[p-1][l] = 1;
        droite = true;
        grille[p-1][l] = 5;
        collision = true;

    }
    if(grille[p-1][l] == 6)
    {
        grille[p][l] = 0;
        grille[p-1][l] = 1;
        gauche = true;
        grille[p-1][l] = 6;
        collision = true;

    }
    if(grille[p-1][l] == 7)
    {
        tempy = l;
        grille[p][l] = 0;
        haut = true;
        p--;
        grille[p][l] = 7;
        collision = true;

    }
    if(grille[p-1][l] == 8)
    {
        grille[p][l]= 0;
        grille[p-1][l] = 1;
        grille[p-1][l] = 8;
        bas = true;
        p--;
        collision = true;

    }
}
