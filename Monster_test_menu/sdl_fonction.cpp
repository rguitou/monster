#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "monster.h"
#include "sdl_fonction.h"
using namespace std;


SDL_Surface *
load_image( string filename )
{
    //Temporary storage for the image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;
    //Load the image
    loadedImage = IMG_Load( filename.c_str() );
    //If nothing went wrong in loading the image
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old image
        SDL_FreeSurface( loadedImage );
    }
    //Return the optimized image
    return optimizedImage;
}

SDL_Surface *
loadImageWithColorKey(string filename, int r, int g, int b)
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old image
        SDL_FreeSurface( loadedImage );

        //If the image was optimized just fine
        if( optimizedImage != NULL )
        {
            //Map the color key
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, r, g, b );

            //Set all pixels of color R 0, G 0xFF, B 0xFF to be transparent
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }
    //Return the optimized image
    return optimizedImage;
}

void
applySurface(int x, int y, SDL_Surface* source,
             SDL_Surface* destination, SDL_Rect* clip)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface( source, clip, destination, &offset );
}

void loadLevel(TGrille grille, SDL_Surface* screen, SDL_Surface* monsterRedSprite,
               SDL_Rect lectureMonsterRed, SDL_Rect lectureMonsterBlue, SDL_Rect livre , SDL_Rect glace, SDL_Rect flecheD,
               SDL_Rect flecheG, SDL_Rect flecheH , SDL_Rect flecheB )
{
    int x, y;
    for(int i = 0; i < TAILLE_WIDTH; i++ )
    {
        for(int j = 0; j < TAILLE_HEIGHT; j++)
        {
            getPixelFromCase(i, j, x, y);
            switch (grille[i][j]) {
            case 1:
                applySurface( x , y ,monsterRedSprite, screen, &lectureMonsterRed);
                break;
            case 2:
                applySurface( x , y ,monsterRedSprite, screen, &lectureMonsterBlue);
                break;
            case 3:
                applySurface( x , y ,monsterRedSprite, screen, &livre);
                break;
            case 4:
                applySurface( x , y ,monsterRedSprite, screen, &glace);
                break;
            case 5:
                applySurface( x , y ,monsterRedSprite, screen, &flecheD);
                break;
            case 6:
                applySurface( x , y ,monsterRedSprite, screen, &flecheG);
                break;
            case 7:
                applySurface( x , y ,monsterRedSprite, screen, &flecheH);
                break;
            case 8:
                applySurface( x , y ,monsterRedSprite, screen, &flecheB);
                break;
            default:
                break;
            }
        }
    }
}

bool gagner(TGrille grille)
{
    bool win = false;
    int cpt = 0;
    for(int i = 0; i < TAILLE_WIDTH; i++)
    {
        for(int j = 0; j < TAILLE_HEIGHT; j++)
        {
            if(grille[i][j]==2)
            {
                cpt++;
            }
        }
    }
    if(cpt==0)
    {
        win = true;
        SDL_Delay(100);
        cout << "GAGNER" << endl;
    }

    return win;
}

/*
SDL_Fonction::SDL_Fonction()
{
}
*/
