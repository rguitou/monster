#ifndef SDL_FONCTION_H
#define SDL_FONCTION_H
#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
using namespace std;

SDL_Surface *load_image(string filename);
SDL_Surface *loadImageWithColorKey(string filename, int r, int g, int b);
void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);
void getCaseFromPixel(int &i, int &j, int x, int y);
void changeLevel(int num, TGrille &grille,SDL_Surface* source, SDL_Surface* destination, bool &quit);
void getPixelFromCase(int i, int j, int &x, int &y);
bool gagner(TGrille grille);
void loadLevel(TGrille grille, SDL_Surface* screen, SDL_Surface* monsterRedSprite, SDL_Rect lectureMonsterRed, SDL_Rect lectureMonsterBlue, SDL_Rect livre , SDL_Rect glace, SDL_Rect flecheD,
               SDL_Rect flecheG, SDL_Rect flecheH , SDL_Rect flecheB );
void detectionBas(TGrille &grille, int p, int &l, int &tempy, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas);
void detectionHaut(TGrille &grille, int p,int &tempx, int &tempy, int &l, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas);
void detectionDroite(TGrille &grille, int &p,int &tempx, int &tempy, int &l, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas);
void detectionGauche(TGrille &grille, int &p,int &tempx, int &tempy, int &l, bool &collision, bool &droite, bool &gauche, bool &haut, bool &bas);
#endif // SDL_FONCTION_H
