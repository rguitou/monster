#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include "monster.h"
#include "sdl_fonction.h"
using namespace std;
/****************** Nom de la fonction **********************
* getPixelFromCase                                          *
******************** Auteur , Dates *************************
*  Auteur: Théo Lecoq      Date: 17/11/2016                 *
********************* Description ***************************
* Cette fonction convertit la position d'une case           *
* de la matrice du niveau en coordonnées en pixels          *
*********************** Entrées *****************************
* Cette fonction prend en entrée une case de la matrice de  *
* jeu de coordonées (i,j)                                   *
*********************** Sorties *****************************
* Cette fonction modifie les valeurs de variables vides     *
* afin d'y assigner une valeur en pixel                     *
************************************************************/
void getPixelFromCase(int i, int j, int &x, int &y)
{
    x = (58 * i) + 22;
    y = (52 * j) + 35;
}
/****************** Nom de la fonction **********************
* getCaseFromPixel                                          *
******************** Auteur , Dates *************************
*  Auteur: Théo Lecoq      Date: 17/11/2016                 *
********************* Description ***************************
* Cette fonction convertit la position d'un pixel sur       *
* l'écran en coordonnées en cases de matrices               *
*********************** Entrées *****************************
* Cette fonction prend en entrée la position d'un pixel sur *
* l'écran de jeu de coordonnées (x,y)                       *
*********************** Sorties *****************************
* Cette fonction modifie les valeurs de variables vides     *
* afin d'y assigner le résultat de la conversion pixel/case *
************************************************************/
void getCaseFromPixel(int &i, int &j, int x, int y)
{
    i = ((x-22) / 58);
    j = ((y-35) / 52);
}
/*
 * les cases mesurent toutes 52x58 pixels, 22 représente la marge sur la largeur
 * et 35 représente la marge sur la hauteur
 * */
void changeLevel(int num, TGrille &grille,SDL_Surface* screen, SDL_Surface* endGame, bool & quit)
{
    switch(num)
    {

    case 0:
        grille ={ 0, 0, 0, 1, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 2, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0};
        break;

    case 1:
        grille = { 0, 0, 0, 1, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 2, 0, 0,
                   0, 0, 0, 3, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0};
        break;

    case 2:
        grille ={ 0, 0, 0, 0, 0, 0, 2, 0, 0,
                  0, 0, 4, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 2, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 1, 0, 0, 0, 0, 2, 0};

        break;


    /*case 1:
        grille ={  0, 4, 0, 3, 3, 3, 3, 3, 0,
                   0, 0, 0, 0, 0, 0, 0, 2, 0,
                   3, 1, 0, 0, 5, 0, 0, 2, 3,
                   0, 0, 0, 0, 0, 0, 0, 2, 0,
                   0, 4, 0, 3, 3, 0, 3, 3, 0};
        break;*/
    case 3:
        grille ={ 0, 0, 2, 0, 8, 0, 0, 0, 4,
                  0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 4, 0, 0, 0, 0, 0, 0, 3,
                  0, 0, 0, 0, 1, 0, 0, 2, 3,
                  0, 0, 0, 3, 3, 3, 0, 4, 0};
        break;
    case 4:
        grille ={ 4, 2, 0, 0, 0, 0, 0, 0, 3,
                  0, 0, 3, 0, 0, 2, 0, 0, 0,
                  4, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 3, 0, 0, 4, 0,
                  0, 0, 1, 0, 0, 0, 0, 3, 0};
        break;
    case 5:
        grille ={ 0, 0, 0, 0, 3, 0, 0, 2, 0,
                  0, 0, 2, 0, 0, 0, 0, 0, 0,
                  0, 4, 0, 0, 1, 4, 0, 0, 0,
                  0, 4, 0, 0, 0, 0, 0, 2, 0,
                  0, 0, 0, 0, 3, 4, 0, 0, 0};
        break;
    case 6:
        grille ={ 1, 0, 0, 0, 3, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 2, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 3, 0, 0, 0, 0, 1};
        break;
    case 7:
        grille ={ 1, 0, 8, 0, 0, 0, 0, 5,
                  0, 8, 0, 0, 0, 0, 6, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 6, 0, 0, 0, 0, 0, 7,
                  0, 0, 0, 0, 0, 0, 2, 0};
        break;
    default:
        applySurface(0, 0, endGame, screen, NULL);
        SDL_Flip(screen);
        SDL_FreeSurface(screen);
        SDL_Delay(2000);
        quit = true;
        break;


    }
}

