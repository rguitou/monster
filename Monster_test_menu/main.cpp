#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <sstream>
#include "monster.h"
#include "sdl_fonction.h"
using namespace std;

struct Monster
{
    int x;
    int y;
    int w = 45;
    int h = 47;

};


int main()
{
    bool quit = false;
    bool collision = false;
    bool droite = false;
    bool gauche = false;
    bool haut = false;
    bool bas = false;
    bool restart = false;
    int nLevel = 0;
    SDL_Event event;
    SDL_Surface *screen;
    SDL_Surface *menu;

    SDL_Rect lectureMonsterRed;
    lectureMonsterRed.x=15;
    lectureMonsterRed.y=146;
    lectureMonsterRed.w=45;
    lectureMonsterRed.h=47;

    SDL_Rect lectureMonsterBlue;
    lectureMonsterBlue.x=9;
    lectureMonsterBlue.y=0;
    lectureMonsterBlue.w=64;
    lectureMonsterBlue.h=65;

    SDL_Rect livre;
    livre.x=69;
    livre.y=67;
    livre.w=57;
    livre.h=63;

    SDL_Rect glace;
    glace.x=8;
    glace.y=68;
    glace.w=57;
    glace.h=63;

    SDL_Rect flecheD;
    flecheD.x=107;
    flecheD.y=224;
    flecheD.w=43;
    flecheD.h=38;

    SDL_Rect flecheG;
    flecheG.x=159;
    flecheG.y=224;
    flecheG.w=41;
    flecheG.h=38;

    SDL_Rect flecheH;
    flecheH.x=62;
    flecheH.y=224;
    flecheH.w=42;
    flecheH.h=38;

    SDL_Rect flecheB;
    flecheB.x=15;
    flecheB.y=224;
    flecheB.w=42;
    flecheB.h=38;

    SDL_Init(SDL_INIT_EVERYTHING);
    screen=SDL_SetVideoMode(320,568,32,SDL_SWSURFACE);
    menu=load_image("menu.bmp");
    Monster rouge;
    TGrille grille;
    grille ={ 1, 0, 8, 0, 0, 0, 0, 0, 5,
              0, 8, 0, 0, 0, 0, 0, 3, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 2, 0,
              0, 6, 0, 0, 0, 0, 0, 0, 7};

    /*grille ={ 0, 0, 0, 1, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 2, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0};*/

    while(!quit)
    {
        applySurface(0,0,menu,screen,NULL);
        SDL_Flip(screen);
        SDL_FreeSurface(screen);
        int x,y;
        int p = -1;
        int l = -1;
        int tempx = -1;
        int tempy = -1;
        SDL_PumpEvents();
        int mousex = event.button.x;
        int mousey = event.button.y;
        if(event.type == SDL_MOUSEBUTTONDOWN && mousex > 100 && mousex < 195 && mousey > 263 && mousey < 353 )
        {

            SDL_Surface *endLevel;
            SDL_Surface *endGame;
            SDL_Surface *failScreen;
            endLevel = load_image("winSprite.bmp");
            endGame = load_image("winEndSprite.bmp");
            failScreen = load_image("failScreen.bmp");
            while(!quit)
            {
                SDL_Surface *backgroundPlay;
                backgroundPlay=load_image("background.bmp");
                applySurface(0,0,backgroundPlay,screen,NULL);
                SDL_Surface *monsterRedSprite;
                monsterRedSprite=loadImageWithColorKey("sprite.bmp", 255,255,255);
                loadLevel(grille, screen, monsterRedSprite, lectureMonsterRed, lectureMonsterBlue, livre, glace, flecheD, flecheG, flecheH, flecheB);
                getCaseFromPixel(x, y, event.button.x, event.button.y);
                cout<<"Case de la souris : x : "<<x<<" y : "<<y<<endl;
                cout<<"Coordonés du monster : p : "<<p<<" l : "<<l<<endl;

                if(event.type == SDL_MOUSEBUTTONDOWN && grille[x][y] == 1)
                {
                    p = x;
                    l = y;
                    tempx = x;
                    tempy = y;
                    collision = false;
                    droite = false;
                    gauche = false;
                    haut = false;
                    bas = false;
                    restart = false;

                }


                if(event.type == SDL_MOUSEBUTTONDOWN && event.button.x>67 && event.button.x<118 && event.button.y>503 && event.button.y<560)
                {
                    changeLevel(nLevel, grille, screen, endGame, quit);
                }

                //--DEPLACEMENT GAUCHE--//

                if((event.type == SDL_MOUSEBUTTONUP && p > -1 && x < p && y == l && !collision) || gauche)
                {
                    while(grille[p-1][l]==0 && !restart)
                    {
                        if(!gauche)
                            grille[p][l] = 0;
                        grille[p-1][l]=1;
                        gauche = false;
                        SDL_Delay(45);
                        applySurface(0, 0, backgroundPlay, screen, NULL);
                        loadLevel(grille, screen, monsterRedSprite, lectureMonsterRed, lectureMonsterBlue, livre, glace, flecheD, flecheG, flecheH, flecheB);
                        SDL_Flip(screen);
                        SDL_FreeSurface(screen);
                        p--;
                        if(p<=0)
                        {
                            cout << "PERDU"<<endl;
                            restart = true;
                            applySurface(0,0,failScreen, screen, NULL);
                            SDL_Flip(screen);
                            SDL_Delay(1000);
                            changeLevel(nLevel, grille, screen, endGame, quit);
                        }
                    }
                     detectionGauche(grille, p, tempx, tempy, l, collision, droite, gauche, haut, bas);
                }

                //--DEPLACEMENT BAS--//
                if((event.type == SDL_MOUSEBUTTONUP && l > -1 && y > l && x == p && !collision) || bas)
                {
                    while(grille[p][l+1]==0 && !restart)
                    {
                        if(!bas)
                            grille[p][l] = 0;
                        grille[p][l+1]=1;
                        bas = false;
                        SDL_Delay(45);
                        applySurface(0, 0, backgroundPlay, screen, NULL);
                        loadLevel(grille, screen, monsterRedSprite, lectureMonsterRed, lectureMonsterBlue, livre, glace, flecheD, flecheG, flecheH, flecheB);
                        SDL_Flip(screen);
                        SDL_FreeSurface(screen);
                        l++;
                        if(l>=8)
                        {
                            cout << "PERDU"<<endl;
                            restart = true;
                            applySurface(0,0,failScreen, screen, NULL);
                            SDL_Flip(screen);
                            SDL_Delay(1000);
                            changeLevel(nLevel, grille, screen, endGame, quit);

                        }


                    }
                    detectionBas(grille, p, l, tempy, collision, droite, gauche, haut, bas);
                }

                //--DEPLACEMENT DROITE--//

                if((event.type == SDL_MOUSEBUTTONUP && tempx > -1 && x > tempx && y == l && !collision) || droite)
                {
                    while(grille[tempx+1][l]==0 && !restart)
                    {


                        grille[tempx+1][l]=1;
                        if(!droite)
                            grille[tempx][l] = 0;
                        droite = false;
                        SDL_Delay(45);
                        applySurface(0, 0, backgroundPlay, screen, NULL);
                        loadLevel(grille, screen, monsterRedSprite, lectureMonsterRed, lectureMonsterBlue, livre, glace, flecheD, flecheG, flecheH, flecheB);
                        SDL_Flip(screen);
                        SDL_FreeSurface(screen);
                        tempx++;
                        if(tempx>=5)
                        {
                            cout << "PERDU"<<endl;
                            restart = true;
                            applySurface(0,0,failScreen, screen, NULL);
                            SDL_Flip(screen);
                            SDL_Delay(1000);
                            changeLevel(nLevel, grille, screen, endGame, quit);

                        }


                    }
                    detectionDroite(grille, p, tempx, tempy, l, collision, droite, gauche, haut, bas);

                }


                //--DEPLACEMENT HAUT--//

                if((event.type == SDL_MOUSEBUTTONUP && tempy > -1 && y < tempy && x == p && !collision) || haut)
                {
                    while(grille[p][tempy-1]==0 && !restart)
                    {
                        grille[p][tempy-1]=1;
                        if(!haut)
                            grille[p][tempy] = 0;
                        haut = false;
                        SDL_Delay(45);
                        applySurface(0, 0, backgroundPlay, screen, NULL);
                        loadLevel(grille, screen, monsterRedSprite, lectureMonsterRed, lectureMonsterBlue, livre, glace, flecheD, flecheG, flecheH, flecheB);
                        SDL_Flip(screen);
                        SDL_FreeSurface(screen);
                        tempy--;
                        if(tempy<=0)
                        {
                            cout << "PERDU" <<endl;
                            restart = true;
                            applySurface(0,0,failScreen, screen, NULL);
                            SDL_Flip(screen);
                            SDL_Delay(1000);
                            changeLevel(nLevel, grille, screen, endGame, quit);

                        }


                    }
                    detectionHaut(grille, p, tempx, tempy, l, collision, droite, gauche, haut, bas);
                }

                if(gagner(grille))
                {
                    cout << "GAGNER" << endl;
                    applySurface(0,0,endLevel,screen,NULL);
                    SDL_Flip(screen);
                    SDL_FreeSurface(screen);
                    SDL_Delay(1000);
                    nLevel++;
                    changeLevel(nLevel, grille, screen, endGame, quit);
                }

                SDL_Flip(screen);
                SDL_FreeSurface(screen);
                SDL_PollEvent(&event);
                if(event.type==SDL_QUIT)
                        quit=true;
                SDL_PumpEvents();
                SDL_FreeSurface(backgroundPlay);
                SDL_FreeSurface(monsterRedSprite);

            }

        }


        if(event.type == SDL_MOUSEBUTTONDOWN && mousex > 195 && mousex < 245 && mousey > 353 && mousey < 393 )
        {
            quit = true;
        }


        while(SDL_PollEvent(&event))
            if(event.type==SDL_QUIT)
                quit=true;



    }
    SDL_FreeSurface(screen);
    SDL_FreeSurface(menu);

    return 0;
}

